#!/bin/bash

DIR="../openshift/"


echo "creating meteor bundle.."

/usr/local/bin/meteor bundle bundle.tar.gz
echo "copy and merge into openshift-launchpad.."
tar -xvf bundle.tar.gz -s '/^bundle//' -C $DIR
rm bundle.tar.gz
cd $DIR
rm main.js
rm server/server.js
cp server/server_WORKS.js server/server.js


echo "pushing data to rhc.."
git add .
git commit -a -m 'rhc deploy..'
git push
echo "done!"


# git commit to bitbucket
echo "committing to bitbucket.."
git add .
git commit -a -m 'autodeploy'
git push

