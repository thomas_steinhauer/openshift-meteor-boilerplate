Meteor.Router.add({
    '/':'home',
    '/contact':'contact',
    '*':'404'
})

// active-states for bootstrap-navbar
Template.navbar.helpers({
    isActive:function (path) {
        return (Meteor.Router.page() == path) ? 'active' : '';
    }
});
